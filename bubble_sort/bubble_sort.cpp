#include<iostream>
#include<cstdio>
#include<vector>

using namespace std;

void bubble_sort(vector<int>& arr) {
  int n=arr.size();
  for(int i=0;i<n;++i) {
    for(int j=0;j<n-i-1;++j) {
      if(arr[j]>arr[j+1]) // find max of nearby pair and swap
	swap(arr[j+1],arr[j]);
    }
  }
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);

  vector<int> arr;
  int n,t;

  
  cin>>n;
  for(int i=0;i<n;++i) {
    cin>>t;
    arr.push_back(t);
  }
  bubble_sort(arr);
  for(int i=0;i<n;++i) {
    cout<<arr[i]<<endl;
  }

  return 0;
}
