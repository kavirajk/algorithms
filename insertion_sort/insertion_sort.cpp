#include<iostream>
#include<cstdio>


using namespace std;

void insertion_sort(int *arr,int n) {
  for(int i=0;i<n;++i) {
    int k=arr[i]; // choosing key
    int j=i-1;
    while(j>=0 && arr[j]>k) { // searching for proper location for the key
      arr[j+1]=arr[j];
      --j;
    }
    arr[j+1]=k; // inserting key in proper position
  }
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);
  int arr[MAX];
  int n;
  cin>>n;
  for(int i=0;i<n;++i) {
    cin>>arr[i];
  }
  insertion_sort(arr,n);
  for(int i=0;i<n;++i) {
    cout<<arr[i]<<endl;
  }
  return 0;
}
