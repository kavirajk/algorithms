#include<iostream>
#include<cstdio>
#include<vector>
#include<iterator>

using namespace std;

int count_split(vector<int>& left,vector<int>& right,vector<int>& full) {
  int lenL,lenR,lenF,count_s;
  lenL=left.size();
  lenR=right.size();
  lenF=lenL+lenR;

  count_s=0;

  int i,j,k;
  i=j=0;

  for(k=0;k<lenF && i<lenL && j<lenR;++k) {
    if(left[i]<right[j]) {
      full[k]=left[i++];
    } else {
      full[k]=right[j++];
      count_s+=lenL-i;
    }
  }
  while(i<lenL)
    full[k++]=left[i++];
  while(j<lenR)
    full[k++]=right[j++];
  return count_s;
}

int inversions_count(vector<int> &arr) {
  int n=arr.size();
  if(n<=1)
    return 0;

  vector<int> left(arr.begin(),arr.begin()+n/2);
  vector<int> right(arr.begin()+n/2,arr.end());

  int count_l=inversions_count(left);
  int count_r=inversions_count(right);
  int count_s=count_split(left,right,arr);
  return count_l+count_r+count_s;
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);

  int n;
  cin>>n;
  vector<int> arr(n);

  for(int i=0;i<n;++i) {
    cin>>arr[i];
  }

  cout<<inversions_count(arr)<<endl;
  return 0;
}
