#include<iostream>
#include<cstdio>
#include<vector>
#include<iterator>

using namespace std;

void merge(vector<int> &left,vector<int> &right,vector<int>& full) {
  int lenL,lenR,lenF;
  lenL=left.size();
  lenR=right.size();
  lenF=lenL+lenR;

  int i,j,k;
  i=j=0;

  for(k=0;k<lenF && i<lenL && j<lenR ;++k) { // Actual merge step
    if(left[i]<right[j]) {
      full[k]=left[i];
      ++i;
    } else {
      full[k]=right[j];
      ++j;
    }
  }
  // What if either one of left or right array is empty??
  while(i<lenL){
    full[k++]=left[i++];
  }
  while(j<lenR) {
    full[k++]=right[j++];
  }
}

void merge_sort(vector<int>& arr) {
  int n=arr.size();
  if(n<=1) // if array contains 1 or 0 elemenst its already sorted
    return;

  vector<int> left(arr.begin(),arr.begin()+n/2);
  vector<int> right(arr.begin()+n/2,arr.end());

  merge_sort(left); // Conquer
  merge_sort(right);
  merge(left,right,arr); //Combine
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);

  int n,t;
  vector<int> arr;
  cin>>n;

  for(int i=0;i<n;++i) {
    cin>>t;
    arr.push_back(t);
  }

  merge_sort(arr);

  for(int i=0;i<n;++i){
    cout<<arr[i]<<endl;
  }

  return 0;
}
