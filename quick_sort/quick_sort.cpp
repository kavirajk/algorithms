/*
  TASK   : Sorting given array of integers using randomized quick sort
  RUNTIME: O(nlogn) "on average"
  AUTHOR : Kaviraj
*/

#include<iostream>
#include<cstdio>
#include<vector>
#include<algorithm>
#include<iterator>
//#include<cmath>

using namespace std;

int choosePivot(int left,int right) {
  int pivot=left+rand()%(right-left+1); // any random pivot within limit left to right
  return pivot;
}

int partition(vector<int>& v,int left,int right) {
  int i,j;
  i=left+1;
  int p=v[left];
  for(j=i;j<=right;++j) {
    if(v[j]<p) {
      swap(v[i],v[j]);
      ++i;
    }
  }
  swap(v[left],v[i-1]);
  return i-1;
}

void quick_sort(vector<int>& v,int left,int right) {
  size_t len=right-left+1;
  if(len<=1)
    return;
  int p=choosePivot(left,right);
  swap(v[left],v[p]);
  p=partition(v,left,right);
  quick_sort(v,left,p-1);
  quick_sort(v,p+1,right);
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);

  vector<int> arr;
  copy(istream_iterator<int>(cin),istream_iterator<int>(),back_inserter(arr));
  quick_sort(arr,0,arr.size()-1);
  copy(arr.begin(),arr.end(),ostream_iterator<int>(cout,"\n"));

  return 0;
}
