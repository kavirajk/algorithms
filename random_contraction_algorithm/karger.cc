#include<iostream>
#include<cstdio>
#include<vector>
#include<sstream>
#include<algorithm>
#include<iterator>
#include<utility>
#include<cmath>
#include<set>
#include<cstdlib>
#include<climits>

#define MAX 201

using namespace std;

typedef set<int> si;
typedef pair<int,si > psi;
typedef vector<psi > vpsi;

class karger_graph {
public:
  size_t edges_size() const;
  size_t nodes_size() const;
  karger_graph& remove_self_loops();
  karger_graph& merge_vertex(size_t u,size_t v);
  karger_graph(vpsi adj);
  void print();
private:
  vpsi adjList;
};

karger_graph:: karger_graph(vpsi adj):adjList(adj) {
  
}

size_t karger_graph::edges_size() const {
  size_t s=0;
  for(auto it=adjList.begin();it!=adjList.end();++it) {
    s+=it->second.size();
  }
  return s;
}

size_t karger_graph::nodes_size() const {
  return adjList.size();
}

karger_graph& karger_graph::remove_self_loops() {
  vpsi::iterator it;
  for(it=adjList.begin();it!=adjList.end();++it) {
    it->second.erase(it->first);
  }
  return *this;
}

karger_graph& karger_graph::merge_vertex(size_t u,size_t v) {
  size_t v1,v2;
  psi p1,p2,ed1,ed2;
  //  p1=p2=NULL;
  vpsi::iterator it;
  for(it=adjList.begin();it!=adjList.end();++it) { // finding two nodes for merge
    if(it->first==u) {
      p1=*it;
    }
    if(it->first==v) {
    p2=*it;
    }
  }
  //  assert(p1!=NULL && p2!=NULL);
  if(p1.second.size()>p2.second.size()) { // finding the bigger size nodes
    ed1=p1;
    ed2=p2;
    v1=p1.first;
    v2=p2.first;
  } else {
    ed1=p2;
    ed2=p1;
    v1=p2.first;
    v2=p1.first;
  }
  si::iterator its;
  for(its=ed2.second.begin();its!=ed2.second.end();++its) { // copying smaller node's edge to bigger node
    ed1.second.insert(*its);
  }

  for(it=adjList.begin();it!=adjList.end();++it) { // erase all edges involving smaller node and replacing it with bigger node
    it->second.erase(ed2.first);
    it->second.insert(ed1.first);
  }
  
  it=find(adjList.begin(),adjList.end(),ed2);
  adjList.erase(it);
  return *this;
}
void karger_graph::print() {
  vpsi::iterator it;
  for(it=adjList.begin();it!=adjList.end();++it) {
    cout<<it->first<<" -> ";
    copy(it->second.begin(),it->second.end(),ostream_iterator<int>(cout,", "));
    cout<<endl;
  }
}

size_t random_contraction_algorithm(karger_graph& g) {
  return 0;
}

int main() {
  srand((unsigned int)time(NULL));
  vpsi adj;
  vpsi::iterator it;
  it=adj.begin();
  string line;
  int node,edge;
  while(getline(cin,line)) {
    stringstream ss(line);
    ss>>node;
    si s;
    while(ss>>edge)
      s.insert(edge);
    adj.push_back(make_pair(node,s));
  }
  int n=adj.size();
  size_t times=n*n*(unsigned int)log((double)n);

  size_t min_cut=UINT_MAX;

  while(times--) {
    cout<<"calculating min-cut\n";
    karger_graph g(adj);
    min_cut=min(min_cut,random_contraction_algorithm(g));
  }
  cout<<"min-cut: "<<min_cut<<endl;

  return 0;
}
