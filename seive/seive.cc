#include <iostream>
#include <cstdio>
#include <bitset>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

vector<int> primes;

void seive(int limit) {
  bitset<10000001> b;
  b.set();
  for(int i=2;i*i<=limit && b[i];++i) {
    for(int j=i*i;j<=limit;j+=i) {
      b[j]=false;
    }
  }

  primes.push_back(2);
  for(int i=3;i*i<=limit;i+=2) {
    if(b[i])
      primes.push_back(i);
  }
}

int main() {
  seive(10000000);
  copy(primes.begin(),primes.end(),ostream_iterator<int>(cout,"\n"));
  return 0;
}
