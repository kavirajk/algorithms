#include<iostream>
#include<cstdio>
#include<vector>

using namespace std;

void selection_sort(vector<int>& arr) {
  int n=arr.size();
  //  cerr<<"n: "<<n<<endl;
  for(int i=0;i<n-1;++i) {
    //cerr<<"i: "<<i<<endl;
    for(int j=i+1;j<n;++j) { // Selecting min element each time
      if(arr[j]<arr[i])
	swap(arr[i],arr[j]);
    }
  }
}

int main() {
  freopen("data.txt","r",stdin);
  freopen("output.txt","w",stdout);
  vector<int> arr;
  int n;
  cin>>n;
  int t;
  for(int i=0;i<n;++i) {
    cin>>t;
    arr.push_back(t);
  }
  //  cerr<<"here!!!\n";
  selection_sort(arr);

  for(int i=0;i<n;++i) {
    cout<<arr[i]<<endl;
  }
  return 0;
}
